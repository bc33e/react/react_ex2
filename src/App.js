import logo from "./logo.svg";
import "./App.css";
import Glass from "./Ex2/Glass";

function App() {
  return (
    <div className="App">
      <Glass />
    </div>
  );
}

export default App;
