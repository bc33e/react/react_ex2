import React, { Component } from "react";
import { data } from "./dataGlasses";
import styles from "./glass.module.css";

export default class Glass extends Component {
  dataGlass = data;
  state = {
    model: {
      url: "",
      name: "",
      price: "",
      desc: "",
    },
  };

  handleGlass = (item) => {
    document.getElementById("model").style.display = "block";
    this.setState({
      model: {
        url: `${item.url}`,
        name: `${item.name}`,
        price: `${item.price}`,
        desc: `${item.desc}`,
      },
    });
  };

  renderGlass = () => {
    return this.dataGlass.map((item, index) => {
      return (
        <img
          onClick={() => this.handleGlass(item)}
          className="p-4"
          key={index}
          src={item.url}
          width={300}
          alt=""
        />
      );
    });
  };
  render() {
    return (
      <div className="container py-5">
        <div
          className="mx-auto inset-x-0"
          style={{ backgroundColor: "rgba(258,175,122,0.7)" }}
        >
          <p className="text-center text-500 text-3xl">TRY GLASS APP ONLINE</p>
        </div>
        <div
          style={{
            backgroundImage: "url(./glassesImage/background.jpg)",
            width: "100",
            height: "100",
            backgroundSize: "cover",
          }}
        >
          <div className="container mx-auto">
            <div className="flex justify-center">
              <div className="relative">
                <img src="./glassesImage/model.jpg" alt="" width={300} />
                <img
                  className="absolute top-24 left-16"
                  src={this.state.model.url}
                  alt=""
                  width={180}
                />
                <div
                  style={{ backgroundColor: "rgba(258,175,122,0.7)" }}
                  id="model"
                  className="absolute inset-x-0 bottom-0 h-32 hidden"
                >
                  <p className="text-blue-400 text-2xl mb-4">
                    {this.state.model.name} - {this.state.model.price} $
                  </p>
                  <p className="text-white">{this.state.model.desc}</p>
                </div>
              </div>
            </div>
            <div
              className="grid grid-cols-6 gap-3 mt-10"
              style={{ background: "white" }}
            >
              {this.renderGlass()}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
